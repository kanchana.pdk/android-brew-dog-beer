package com.example.todolist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.model.TodoListModel

// Adapter abstract class have to extend ViewHolder class
class TodoAdapter(private val listener: TodoClickListener) : RecyclerView.Adapter<TodoListViewHolder>() {

    // Get value from other class
    private val todoList: ArrayList<TodoListModel> = arrayListOf()

    // Generate Holder to manage data
    // and return parent of TodoListViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TodoListViewHolder(parent)

    // Bind method with ViewHolder
    // position is index of item
    override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
        holder.bind(todoList[position], listener)
    }

    override fun getItemCount() = todoList.count()

    fun addListTask(task: TodoListModel) {
        todoList.add(task)
        // Reset todoList Array to update value
        notifyDataSetChanged()
    }
}

interface TodoClickListener {
    fun onItemClick(taskName: String)
}

class TodoListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(

    // Similar to onCreate but change to Inflate
    LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
) {
    // Set text id name "task" with UI
    private val taskName: TextView = itemView.findViewById(R.id.task)
    private val taskCheckbox: CheckBox = itemView.findViewById(R.id.cbTask)

    fun bind(model: TodoListModel, listener: TodoClickListener) {
        taskName.text = model.todo
        itemView.setOnClickListener {
            listener.onItemClick(taskName.text.toString())
        }
        taskCheckbox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {

            }
        })

        itemView.visibility = if (model.isComplete) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

}
