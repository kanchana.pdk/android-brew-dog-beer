package com.example.todolist.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.todolist.R
import com.example.todolist.model.BeerModel
import com.example.todolist.service.BeerManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.*

class BeerActivity : Fragment(), BeerInterface {

    private val presenter = BeerPresenter(this)

    companion object {
        fun getInstant(): BeerActivity = BeerActivity()
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_beer)
//        presenter.getBeerApi()
//        setView()
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_beer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getBeerApi()
        setView()
    }

    override fun setBeer(beerItem: BeerModel) {
        tvBeer.text = "${beerItem.name}"
        tvAbv.text = "${beerItem.abv}"
        tvDescription.text = "${beerItem.description}"
        Picasso.get().load(beerItem.imageUrl)
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(imgBeer)
    }

    private fun setView() {
        btnRefresh.setOnClickListener {
            presenter.getBeerApi()
        }
    }
}
