package com.example.todolist

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.todolist.model.TodoListModel

class TodoDertail : AppCompatActivity() {

    companion object {
        const val EXTRA_KEY_TITLE = "TITLE_NAME"
        const val EXTRA_KEY_MODEL = "MODEL"
        fun startActivity(context: Context, titleName: String) = context.startActivity(Intent(context, TodoDertail::class.java).also {
            it.putExtra(EXTRA_KEY_TITLE, titleName)
//            it.putExtra(EXTRA_KEY_MODEL, model)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo_dertail)
        title = intent.getStringExtra(EXTRA_KEY_TITLE)

//        var model: TodoListModel = intent.getParcelableExtra(EXTRA_KEY_MODEL)
//        println("bobo > $model")
    }


}
