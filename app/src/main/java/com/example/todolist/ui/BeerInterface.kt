package com.example.todolist.ui

import com.example.todolist.model.BeerModel

interface BeerInterface {
    fun setBeer(beerList: BeerModel)
}