package com.example.todolist.ui

import com.example.todolist.model.BeerModel
import com.example.todolist.service.BeerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerPresenter(val view: BeerInterface) {
    fun getBeerApi() {
        // Create instance to build service
        // Enqueue to getRandomBeer, then wait api to getting data on List BeerModel
        BeerManager().createService().getRandomBeer().enqueue(object : Callback<List<BeerModel>>{
            override fun onFailure(call: Call<List<BeerModel>>, t: Throwable) {
                println("FAILED")
            }

            override fun onResponse(
                call: Call<List<BeerModel>>,
                response: Response<List<BeerModel>>
            ) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        view.setBeer(this[0])
                    }
                }
            }
        })
    }
}