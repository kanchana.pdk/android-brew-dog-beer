package com.example.todolist.fragment_sample.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.todolist.R
import com.example.todolist.fragment_sample.data.FragmentModel


class SectionsPagerAdapter( val fmList: List<FragmentModel>, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return fmList[position].fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fmList[position].tabName
    }

    override fun getCount(): Int {
        return fmList.count()
    }
}