package com.example.todolist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolist.model.TodoListModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // lateinit is providing value after
    private lateinit var todoAdapter: TodoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setView()
    }

    private fun setView() {
//        val todoListModel = TodoListModel(false, "bobo")
//        btnAdd.setOnClickListener {
//            TodoDertail.startActivity(this, etInputTask.text.toString(), todoListModel)
//        }

        val listener = object : TodoClickListener {
            override fun onItemClick(taskName: String) {
                TodoDertail.startActivity(this@MainActivity, taskName)
                println("task: $taskName")
            }
        }
        todoAdapter = TodoAdapter(listener)

        rvTodo.adapter = todoAdapter
        rvTodo.layoutManager = LinearLayoutManager(this)
        rvTodo.itemAnimator = DefaultItemAnimator()

        btnAdd.setOnClickListener {
            val todoModel: TodoListModel = getAddModel(etInputTask.text.toString())
            todoAdapter.addListTask(todoModel)
            etInputTask.text = null
        }
    }

    private fun getAddModel(name: String): TodoListModel {
        return TodoListModel(false, name)
    }

}
