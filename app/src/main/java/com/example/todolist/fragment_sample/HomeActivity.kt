package com.example.todolist.fragment_sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.todolist.R
import com.example.todolist.fragment_sample.data.FragmentModel
import com.example.todolist.fragment_sample.ui.main.PlaceholderFragment
import com.example.todolist.fragment_sample.ui.main.ProfileFragment
import com.example.todolist.fragment_sample.ui.main.SectionsPagerAdapter
import com.example.todolist.ui.BeerActivity
import kotlinx.android.synthetic.main.activity_home_x.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_x)
        setView()
    }

    fun setView() {
        val tabList = listOf<FragmentModel>(
            FragmentModel("Home", PlaceholderFragment.newInstance(1)),
            FragmentModel("Profile", ProfileFragment.getInstant()),
            FragmentModel("Beer", BeerActivity.getInstant())
        )

        val sectionsPagerAdapter = SectionsPagerAdapter(tabList, supportFragmentManager)
        view_pager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(view_pager)

    }
}