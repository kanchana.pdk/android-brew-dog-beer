package com.example.todolist.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TodoListModel (
    var isComplete: Boolean,
    var todo: String
) : Parcelable