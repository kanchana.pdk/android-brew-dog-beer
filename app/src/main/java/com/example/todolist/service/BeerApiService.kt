package com.example.todolist.service

import com.example.todolist.model.BeerModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface BeerApiService {

    @GET("v2/beers/random")
    fun getRandomBeer(): Call<List<BeerModel>>

}